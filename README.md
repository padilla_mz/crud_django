# CRUD Django

Pasos para arrancar la applicación.

1. Clonar este repositorio

    `$ git clone git@bitbucket.org:padilla_mz/crud_django.git`

    `$ cd crud_django`

2. (Opcional) Crear entorno virtual & activarlo (venv)

    `$ python3 -m venv venv`

    `$ source venv/bin/activate`

3. Instalar paquetes requeridos por la applicación

    `$ pip install -r requirements.txt`

4. Crear base de datos

    `$ python manage.py makemigrations`

    `$ python manage.py migrate`

5. Iniciar el servidor

    `$ python manage.py runserver`

