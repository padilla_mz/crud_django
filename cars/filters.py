from django_filters import FilterSet, DateFilter, CharFilter
from django.forms.widgets import TextInput
from .models import *


class CarFilter(FilterSet):

    model = CharFilter(lookup_expr='icontains', label=False, widget=TextInput(attrs={'placeholder': 'Modelo', 'class': 'input'}))
    brand = CharFilter(lookup_expr='icontains', label=False, widget=TextInput(attrs={'placeholder': 'Marca', 'class': 'input'}))

    # category = django_filters.ChoiceFilter()

    class Meta:
        model = Car
        # fields = '__all__'
        # exclude = ['']

        # extra fields and order
        fields = ['model', 'brand']