from django.db import models

# Create your models here.
class Car(models.Model):

    # String
    model = models.CharField(max_length=200, verbose_name="Modelo", null=True)
    brand = models.CharField(max_length=200, verbose_name="Marca", null=True)
    year = models.IntegerField(verbose_name="Año", null=True)

    ## Float point
    price = models.DecimalField(max_digits=19, decimal_places=2)


    ## Date
    date_created =  models.DateTimeField(auto_now_add=True, null=True)
    date_updated =  models.DateTimeField(auto_now=True, null=True)


    def __str__(self):
        return f"{self.brand} {self.model}"