from django import forms
from .models import Car
from datetime import datetime


def possible_years(first_year_in_scroll, last_year_in_scroll):
    p_year = []
    for i in range(first_year_in_scroll, last_year_in_scroll, -1):
        p_year_tuple = str(i), i
        p_year.append(p_year_tuple)
    return p_year

class CarForm(forms.ModelForm):
    year = forms.ChoiceField(choices=possible_years(((datetime.now()).year + 1), 1900), label='Año')

    class Meta:
        model = Car
        # fields = ['title', 'model', 'price']
        fields = '__all__'