from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages

from django.db.models import Avg
from .models import *
from .filters import CarFilter
from .forms import CarForm



def listado(request):
    if request is None:
        cars = Car.objects.all()
        promedio = cars.aggregate(Avg('price'))
        obs = {
            'cars': cars,
            'promedio': promedio
        }
    else:
        cars = Car.objects.all()
        promedio = cars.aggregate(Avg('price'))

        filter = CarFilter(request.GET, queryset=cars)
        cars = filter.qs

        obs = {
            'cars': cars,
            'filter': filter,
            'promedio': promedio
        }

    return render(request, 'cars/listado.html', obs)


def create(request):
    action = 'create' # variable para validar que view estamos
    form = CarForm()

    if request.method == 'POST':
        form = CarForm(request.POST)

        if form.is_valid():
            item = form.save()
            messages.success(request, f'{item} creado con exito ...')
            return redirect('items_listado')


    obs =  {'action':action, 'form':form}
    return render(request, 'cars/car_form.html', obs)

def update(request, id):
    action = 'update'
    item = get_object_or_404(Car, id=id)
    form = CarForm(instance=item)

    if request.method == 'POST':
        form = CarForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            messages.success(request, f'{item} actualizado con exito ...')
            return redirect('items_listado')
    
    obs =  {'action':action, 'form':form}
    return render(request, 'cars/car_form.html', obs)

def delete(request):
    print (request.POST['id'])

    if request.method == 'POST':
        item = Car.objects.get(id=request.POST['id'])
        
        item.delete()
        messages.success(request, 'Auto eliminado con exito ...')
        return redirect('items_listado')