from django.urls import path
from . import views

urlpatterns = [
    path('', views.listado, name='items_listado'), 
    path('listado/crear', views.create, name='create_item'), 
    path('listado/actualizar/<str:id>', views.update, name='update_item'), 
    path('listado/destruir', views.delete, name='delete_item'), 

]
